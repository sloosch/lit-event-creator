"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const NOOP = () => {
};
function bracket(setupFn) {
    return (target, t) => {
        const originalConnect = target['connectedCallback'] || NOOP;
        const originalDisconnect = target['disconnectedCallback'] || NOOP;
        let tearDownFn = NOOP;
        target['connectedCallback'] = function () {
            tearDownFn = setupFn.call(this, target, t);
            originalConnect.apply(this, arguments);
        };
        target['disconnectedCallback'] = function () {
            tearDownFn.call(this);
            originalDisconnect.apply(this, arguments);
        };
    };
}
function listenTo(creator) {
    return bracket(function (target, name) {
        return creator.listen(this, (data, event) => target[name].call(this, data, event));
    });
}
exports.listenTo = listenTo;
function provideEventListener(eventName) {
    return bracket(function (target, name) {
        return target[name].call(this, (eventTarget, listener) => {
            eventTarget.addEventListener(eventName, listener);
            return () => eventTarget.removeEventListener(eventName, listener);
        });
    });
}
exports.provideEventListener = provideEventListener;
function eventCreatorFactory(namespace, separator = '-') {
    return (name) => {
        const uniqName = namespace + separator + name;
        return {
            create(data) {
                return new CustomEvent(uniqName, {
                    detail: data,
                    bubbles: true,
                    composed: true
                });
            },
            listen(node, listener) {
                const internalListener = (event) => {
                    listener(event.detail, event);
                };
                node.addEventListener(uniqName, internalListener);
                return () => node.removeEventListener(uniqName, internalListener);
            }
        };
    };
}
exports.eventCreatorFactory = eventCreatorFactory;
