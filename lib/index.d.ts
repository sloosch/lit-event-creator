export interface TeardownFn {
    (): void;
}
export interface EventCreator<T> {
    create(data: T): CustomEvent<T>;
    listen(node: Element, listener: (data: T, event: Event) => void): TeardownFn;
}
export declare function listenTo<T>(creator: EventCreator<T>): (target: any, t: string) => void;
export interface ProvideEventListener {
    (target: EventTarget, listener: EventListener): TeardownFn;
}
export declare function provideEventListener(eventName: string): (target: any, t: string) => void;
export interface EventCreatorFactory<M> {
    <K extends keyof M>(name: K): EventCreator<M[K]>;
}
export declare function eventCreatorFactory<M>(namespace: string, separator?: string): EventCreatorFactory<M>;
