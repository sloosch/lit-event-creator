export interface TeardownFn {
    (): void;
}

const NOOP = () => {
};

export interface EventCreator<T> {
    create(data: T): CustomEvent<T>;

    listen(node: Element, listener: (data: T, event: Event) => void): TeardownFn;
}


function bracket<T>(setupFn: (target: HTMLElement, t: T) => TeardownFn): (target: any, t: T) => void {
    return (target: HTMLElement, t: T) => {
        const originalConnect = target['connectedCallback'] || NOOP;
        const originalDisconnect = target['disconnectedCallback'] || NOOP;
        let tearDownFn = NOOP;

        target['connectedCallback'] = function () {
            tearDownFn = setupFn.call(this, target, t);
            originalConnect.apply(this, arguments);
        };

        target['disconnectedCallback'] = function () {
            tearDownFn.call(this);
            originalDisconnect.apply(this, arguments);
        };
    };
}

export function listenTo<T>(creator: EventCreator<T>) {
    return bracket<string>(function (target: HTMLElement, name: string) {
        return creator.listen(this, (data, event) => target[name].call(this, data, event));
    });
}

export interface ProvideEventListener {
    (target: EventTarget, listener: EventListener): TeardownFn;
}

export function provideEventListener(eventName: string) {
    return bracket<string>(function (target: HTMLElement, name: string) {
        return target[name].call(this, (eventTarget: EventTarget, listener) => {
            eventTarget.addEventListener(eventName, listener);
            return () => eventTarget.removeEventListener(eventName, listener);
        });
    });
}

export interface EventCreatorFactory<M> {
    <K extends keyof M>(name: K): EventCreator<M[K]>;
}

export function eventCreatorFactory<M>(namespace: string, separator: string = '-'): EventCreatorFactory<M> {
    return <K extends keyof M>(name: K) => {
        const uniqName = namespace + separator + name;
        return {
            create(data: M[K]): CustomEvent<M[K]> {
                return new CustomEvent(uniqName, {
                    detail: data,
                    bubbles: true,
                    composed: true
                });
            },
            listen(node, listener) {
                const internalListener = (event) => {
                    listener(event.detail, event);
                };

                node.addEventListener(uniqName, internalListener);

                return () => node.removeEventListener(uniqName, internalListener);
            }
        };
    };
}
